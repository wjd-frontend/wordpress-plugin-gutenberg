/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

import './anchormenu/anchormenu';
import './section/section';
import './button/button';
import './card/card';
import './post-card/postcard';
import './project-card/projectcard';
import './position-card/positioncard';
import './download/download';
import './cite/cite';
import './hero/hero';
import './calendarwidget/calendarwidget';
import './carousel/carousel';
import './carouselcard/carouselcard';
import './posts/posts';
import './blacklist';
import './corefilter';
import './hero-cta/hero-cta';
import './hero-cta-slide/hero-cta-slide';
