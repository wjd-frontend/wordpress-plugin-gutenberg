const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { 
    RichText,
    InnerBlocks,
    InspectorControls,
    BlockControls,
    AlignmentToolbar,
} = wp.blockEditor;
const { 
    Icon,
    CheckboxControl,
    PanelBody,
    PanelRow,
} = wp.components;

const iconEl = () => (
	<Icon
		icon={
			<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" role="img" aria-hidden="true" focusable="false">
                <path d="M19 6H6c-1.1 0-2 .9-2 2v9c0 1.1.9 2 2 2h13c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm-4.1 1.5v10H10v-10h4.9zM5.5 17V8c0-.3.2-.5.5-.5h2.5v10H6c-.3 0-.5-.2-.5-.5zm14 0c0 .3-.2.5-.5.5h-2.6v-10H19c.3 0 .5.2.5.5v9z"></path>
            </svg>
		}
	/>
);


registerBlockType( 'wjd/carousel', {
    title: __( 'Karusell' ),
    icon: iconEl,
    category: 'common',
    keywords: [
        'karussell',
        'slider',
        'swiper'
    ],
    attributes: {
        title: {
            type: 'string',
        },    
        alignment: {
            type: 'string',
            default: 'left',
        },
        showNav: {
            type: 'boolean',
            default: true
        }
    },
    edit: function( props ) {
        const {
            className,
            attributes: {
                alignment,
                title,
                showNav
            },
            setAttributes,
        } = props;
        const onChangeAlignment = ( newAlignment ) => {
            props.setAttributes( { alignment: newAlignment === undefined ? 'none' : newAlignment } );
        };
        const ALLOWED_BLOCKS = [ 'wjd/carouselcard' ];
        return (
            <div className={ className }>
                <InspectorControls key='inspector'>
                    <PanelBody title="Karteneigenschaften">
                        <PanelRow>
                            <CheckboxControl
                                label="Zeige Navigation"
                                checked={ showNav }
                                onChange={ (value) => { setAttributes( { showNav: value } )} }
                            />
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>
                <BlockControls>
                    <AlignmentToolbar
                        value={ alignment }
                        onChange={ onChangeAlignment }
                    />
                </BlockControls> 
                <RichText
                    tagName="h3"
                    style={ { textAlign: alignment } }
                    placeholder={ __( 'Karusell-Überschrift' ) }
                    value={ title }
                    onChange={ ( value ) => setAttributes( { title: value } ) }
                />
                <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS }/>
            </div>
        );
    },
    save: function( props ) {
        return (
            <div>
                <InnerBlocks.Content />
            </div>
        );
    },
} );