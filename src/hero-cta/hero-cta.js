import './editor.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { 
    RichText,
    InnerBlocks,
    InspectorControls,
} = wp.blockEditor;
const {
    PanelBody,
    PanelRow,
	TextControl,
	SelectControl,
	FormToggle,
} = wp.components;

registerBlockType( 'wjd/hero-cta', {
    title: __( 'Hero mit CTA' ),
    icon: 'format-gallery',
    category: 'common',
    keywords: [
        'hero',
        'slider',
        'cta',
        'call to action',
    ],
    attributes: {
        headline: {
            type: 'string'
        },
        subheadline: {
            type: 'string'
        },
        heroHeight: {
            type: 'string'
        },
        heroDetailHorizontalSelect: {
            type: 'string'
        },
        heroDetailVerticalSelect: {
            type: 'string'
        },
        heroImageSize: {
            type: 'boolean',
            default: true
        }
    },
    edit({ attributes, className, setAttributes }) {
        
        const {
            headline,
            subheadline,
            heroHeight,
            heroDetailHorizontalSelect,
            heroDetailVerticalSelect,
            heroImageSize
        } = attributes;

        const ALLOWED_BLOCKS = [ 'wjd/hero-cta-slide' ];

        return (
            <div>
                <InspectorControls>
                    <PanelBody title="Hero Einstellungen">
                        <PanelRow>
                            <TextControl
                                label="Hero Höhe in px eingeben"
                                value={ heroHeight }
                                type="number"
                                onChange={ (value) => {
                                    setAttributes( { heroHeight: value } )
                                } }
                            />
                        </PanelRow>
                        <PanelRow>
                            <SelectControl
                                label="Horizontalen Bidlausschnitt wählen"
                                value={ heroDetailHorizontalSelect }
                                options={ [
                                    { label: 'Rechts', value: 'right' },
                                    { label: 'Zentriert', value: 'center' },
                                    { label: 'Links', value: 'left' },
                                ] }
                                onChange={ ( value ) => {
                                    setAttributes( { heroDetailHorizontalSelect: value } );
                                } }
                            />
                        </PanelRow>
                        <PanelRow>
                            <SelectControl
                                label="Vertikalen Bidlausschnitt wählen"
                                value={ heroDetailVerticalSelect }
                                options={ [
                                    { label: 'Oben', value: 'top' },
                                    { label: 'Mittig', value: 'center' },
                                    { label: 'Unten', value: 'bottom' },
                                ] }
                                onChange={ ( value ) => {
                                    setAttributes( { heroDetailVerticalSelect: value } );
                                } }
                            />
                        </PanelRow>
                        <PanelRow className={'image-size'}>
                            <FormToggle
                                label="Bild füllend darstellen"
                                checked={ heroImageSize }
                                onChange={ () => {
                                    setAttributes( { heroImageSize: !heroImageSize } )
                                } }
                            />
                            <span>Bild füllend darstellen</span>
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>
                <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS }/>
                <RichText
                    tagName="h3"
                    className= "hero__subheadline"
                    placeholder='Hero obere Überschrift'
                    value={subheadline}
                    onChange={ ( value ) => setAttributes( { subheadline: value } ) }
                />
                <RichText
                    tagName="h1"
                    className= "hero__headline"
                    placeholder='Hero Überschrift'
                    value={headline}
                    onChange={ ( value ) => setAttributes( { headline: value } ) }
                />
            </div>

        );
    },

    save: function( props ) {
        return (
            <div>
                <InnerBlocks.Content />
            </div>
        );
    },
} );