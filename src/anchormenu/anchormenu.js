const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'wjd/anchormenu', {
    title: __( 'Ankermenü' ),
    icon: 'admin-links',
    category: 'common',
    keywords: [
        'anker',
        'menu',
        'menü',
    ],
	attributes: {
		sections: {
			type: 'array',
		},
	},
    edit: function() {
        return (
            <div class='wjd-anchormenu' >
                <p>Ankermenü</p>
            </div>
        );
    },
	save: function( props ) {
        let sectionHeadlines = [];
        jQuery('div.wp-block-wjd-group h3.section-headline').each(function(k, value) {
            if (value.innerText.trim().length > 0) {
                sectionHeadlines.push({
                    'key': k,
                    'text': value.innerText
                });
            }
        })
        props.attributes.sections = sectionHeadlines;
		return ( props );
	}
} );