import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { InspectorControls, RichText } = wp.blockEditor; // Import registerBlockType() from wp.blocks
const { SelectControl, PanelBody, PanelRow, RangeControl, CheckboxControl} = wp.components;
const { Component, Fragment } = wp.element;



class mySelectCategories extends Component {
    // Method for setting the initial state.
    static getInitialState( selectedCategory, selectedStyle, perRow, showLoadMore ) {
        return {
            categories: [],
            selectedCategory: selectedCategory,
            category: {},
            postList: [],
            selectedStyle: selectedStyle,
            perRow: perRow,
            showLoadMore: showLoadMore
        };
    }
    // Constructing our component. With super() we are setting everything to 'this'.
    // Now we can access the attributes with this.props.attributes
    constructor() {
        super( ...arguments );
        this.state = this.constructor.getInitialState( 
            this.props.attributes.selectedCategory,
            this.props.attributes.selectedStyle,
            this.props.attributes.perRow,
            this.props.attributes.showLoadMore
        );
        // Bind so we can use 'this' inside the method.
        this.getOptions = this.getOptions.bind(this);
        // Load posts.
        this.getOptions();
        this.onChangeSelectCategory = this.onChangeSelectCategory.bind(this);
        this.setPostLimit = this.setPostLimit.bind(this);
        this.setPerRow = this.setPerRow.bind(this);
        this.onChangeSelectStyle = this.onChangeSelectStyle.bind(this);
        this.onChangeShowLoadMore = this.onChangeShowLoadMore.bind(this);
    }
    getOptions() {
        return ( new wp.api.collections.Categories() ).fetch({ data: { per_page: 100 } }).then( ( categories ) => {            
            if( categories && 0 !== this.state.selectedCategory ) {                
                const category = categories.find( ( item ) => { return item.id == this.state.selectedCategory } );                
                this.setState( { category, categories } );
            } else {
                this.setState({ categories });
            }
        });
    }
    onChangeSelectCategory( value ) {        
        const category = this.state.categories.find( ( item ) => { return item.id == parseInt( value ) } );        
        if (category) {
            this.props.setAttributes( {
                selectedCategory: parseInt( value )
            });
            this.setState( { selectedCategory: parseInt( value ), category } );            
        } else {
            this.props.setAttributes( {
                selectedCategory: 0
            });
            this.setState( { selectedCategory: parseInt( value ), category } );
        }
        setTimeout(() => {
            this.renderPostList();
        }, 100);
    }
    onChangeSelectStyle( value ) {
        this.setState( { selectedStyle: value } )
        this.props.setAttributes({
            selectedStyle: value
        });
        setTimeout(() => {
            this.renderPostList();
        }, 100);
    }
    setPostLimit( value ) {
        this.setState( { postLimit: value })
        this.props.setAttributes({
            postLimit: value
        });
        setTimeout(() => {
            this.renderPostList();            
        }, 100);
    }
    setPerRow( value ) {
        this.setState( { perRow: value } )
        this.props.setAttributes({
            perRow: value
        });
        setTimeout(() => {
            this.renderPostList();
        }, 100);
    }
    onChangeShowLoadMore( value ) {
        this.setState( { showLoadMore: value } )
        this.props.setAttributes({
            showLoadMore: value
        });
    }

    componentWillMount() {
        this.renderPostList();
    }

    renderPostList() {
        let filter = { 
            data: {
                'per_page': this.props.attributes.postLimit,
                '_embed': true
            }
        }
        if (this.props.attributes.selectedCategory !== 0) {
            filter.data.categories = this.props.attributes.selectedCategory;
        }
        var postsCollection = new wp.api.collections.Posts();
        postsCollection.fetch(filter).then(p => {
            this.setState({postMax: p.length});
            if (p) {
                if (this.props.attributes.selectedStyle === 'grid') {
                    var rowedPostList = [];
                    var rowCount = 0;
                    p.forEach((value, k) => {
                        if (k % (12 / this.props.attributes.perRow) === 0) {
                            rowCount++;
                        }
                        if (rowedPostList[rowCount] === undefined) {
                            rowedPostList[rowCount] = [];
                        }
                        rowedPostList[rowCount].push(value);
                    })
                    this.setState({ postList: rowedPostList });
                } else if (this.props.attributes.selectedStyle === 'slider' || this.props.attributes.selectedStyle === 'list') {
                    this.setState({ postList: p });
                } else  if (this.props.attributes.selectedStyle === 'masonry') {
                    this.setState({ postList: p });
                    applyMasonry();
                }
            }
        })
    }

    render() {
        let options = [ { value: 0, label: __( 'Wählen Sie eine Kategorie' ) } ];
        let output = '';
        if ( this.state.categories.length > 0 ) {
            this.state.categories.forEach((category) => {
                options.push({value:category.id, label:category.name});
            });
        }
        // Checking if we have anything in the object
        if( this.state.selectedCategory !== 0 ) {
            output = 'Lade spezielle Beiträge';
            
        } else {
            output = 'Lade alle Beiträge';
        }
        return [
            true && ( 
            <Fragment>
                <InspectorControls key='inspector'>
                    <PanelBody title="Optionen">
                        <PanelRow>
                            <SelectControl 
                                onChange={this.onChangeSelectCategory} 
                                value={ this.props.attributes.selectedCategory } 
                                label={ __( 'Wählen Sie eine Kategorie' ) } 
                                options={ options } />
                        </PanelRow>
                        <PanelRow>
                            <SelectControl 
                                onChange={this.onChangeSelectStyle} 
                                value={ this.props.attributes.selectedStyle } 
                                label={ __( 'Wählen Sie einen Style' ) } 
                                options={ [ 
                                    { value: 'list', label: __( 'Liste' ) },
                                    { value: 'grid', label: __( 'Grid' ) },
                                    { value: 'slider', label: __( 'Slider' ) },
                                    { value: 'masonry', label: __( 'Masonry' ) }
                                ] } />
                        </PanelRow>
                        { this.props.attributes.selectedStyle !== 'list' ? (
                            <PanelRow>
                                <SelectControl
                                    onChange={ this.setPerRow }
                                    value={ this.props.attributes.perRow }
                                    label={this.props.attributes.selectedStyle == 'slider' ? ' Sichtbare Elemente im Slider' : ' Spalten'}
                                    options={ [
                                        { value: 12, label: this.props.attributes.selectedStyle == 'slider' ? '1 pro Slide' : '1 Spalte'},
                                        { value: 6, label: this.props.attributes.selectedStyle == 'slider' ? '2 pro Slide' : '2 Spalten' },
                                        { value: 4, label: this.props.attributes.selectedStyle == 'slider' ? '3 pro Slide' : '3 Spalten' },
                                        { value: 3, label: this.props.attributes.selectedStyle == 'slider' ? '4 pro Slide' : '4 Spalten' },
                                        { value: 2, label: this.props.attributes.selectedStyle == 'slider' ? '6 pro Slide' : '6 Spalten' }
                                    ] }
                                />
                            </PanelRow>
                        ) : (<Fragment></Fragment>) }
                        <PanelRow>
                            <RangeControl
                                label="Limit"
                                value={ this.props.attributes.postLimit }
                                onChange={ this.setPostLimit }
                                min={ 1 }
                                max={ 20 }
                            />
                        </PanelRow>
                        { this.props.attributes.selectedStyle !== 'slider' ? (
                            <PanelRow>
                                <CheckboxControl
                                    label="Zeige 'Mehr Laden' Button"
                                    checked={ this.props.attributes.showLoadMore }
                                    onChange={ this.onChangeShowLoadMore }
                                />                                
                            </PanelRow>
                        ) : (<Fragment></Fragment>) }
                    </PanelBody>
                </InspectorControls>
            </Fragment>
            ), 
            <div className={this.props.className}>
                { this.state.postList.length > 0 ? (
                    <Fragment>
                        {
                            outputPostList(this.state.postList, this.props.attributes)
                        }
                        {
                            (this.props.attributes.selectedStyle !== 'slider' && this.props.attributes.showLoadMore) ? (
                                <div className="load-more">Mehr laden</div>
                            ) : (<Fragment></Fragment>)
                        }
                    </Fragment>
                ) : (
                    'Keine Beiträge gefunden'
                )} 
            </div>                
        ]
    }
}

registerBlockType( 'wjd/posts', {
	title: __( 'Beitrags-Kartensammlung' ),
	icon: 'admin-page',
	category: 'common',
	keywords: [
		'posts',
		'beiträge',
		'liste',
		'beitrag',
		'news',
		'archiv',
		'kategorie'
	],
    attributes: {
        selectedCategory: {
            type: 'number',
            default: 0,
        },
        selectedStyle: {
            type: 'string',
            default: 'grid',
        },
        postLimit: {
            type: 'number',
            default: 6,
        },
        perRow: {
            type: 'string',
            default: '4',
        },
        showLoadMore: {
            type: 'boolean',
            default: false
        }
    },
    edit: mySelectCategories,
    save: function( props ) {
        return (props);
    },
} );

function outputPostList(list, props) {
    if (props.selectedStyle === 'grid') {
        return list.map((row) => (
            <div className="row">
                {
                    Array.isArray(row) ? row.map((post) => {
                        return renderSinglePostAsCard(post, props);
                    }) : (<Fragment></Fragment>)
                }
            </div>
        ))
    } else if (props.selectedStyle === 'slider') {
        return <div className="slider">
            <div className="slider-tray row">
                {
                    !Array.isArray(list[list.length - 1]) ? list.map((post) => {
                        return renderSinglePostAsCard(post, props);
                    }) : (<Fragment></Fragment>)
                }
            </div>
        </div>
    } else if (props.selectedStyle === 'masonry') {
        return <div className={`grid grid-${props.perRow}`}>
            <div class="grid-sizer"></div>
            {
                !Array.isArray(list[list.length - 1]) ? list.map((post) => {
                    return renderSinglePostAsCard(post, props);
                }) : (<Fragment></Fragment>)
            }
        </div>
    } else if (props.selectedStyle === 'list') {
        return !Array.isArray(list[list.length - 1]) ? list.map((post) => {
            return renderSinglePostAsListCard(post, props);
        }) : (<Fragment></Fragment>)
    } else {
        return <Fragment></Fragment>
    }
}

function applyMasonry() {
    jQuery('.grid').imagesLoaded( function() {
        jQuery('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true,
            gutter: 20                      
        });     
    } );
    
}

function renderSinglePostAsCard(post, props) {
    let image = '';
    if (post._embedded['wp:featuredmedia']) {
        image = post._embedded['wp:featuredmedia'][0].source_url;
    }
    let cat = '';
    if (post._embedded['wp:term']) {
        cat = post._embedded['wp:term'][0][0].name;
    }
    return (
        <div className={props.selectedStyle == 'masonry' ? 'grid-item' : `col-${props.perRow}`}>
            <div className="v-card">
                { image && image.length > 0 ? (
                    <div class="card__image-wrapper">
                        <figure class="figure">
                            <div class="figure__image-wrapper is-highlighted">
                                <div class="figure__image">
                                    <img src={ image } />
                                </div>
                            </div>
                        </figure>
                    </div>
                ) : (<Fragment></Fragment>) }                                            
                <div class="card__content">
                    <ul class="card__meta-data no-list-style">
                        <li class="card__meta-data-item is-date">
                            { new Date(post.date).toLocaleDateString('de-de', { year:"numeric", month:"numeric", day:"numeric"}) }
                        </li>
                        { cat && cat.length > 0 ? (
                            <li class="card__meta-data-item is-text">
                                { cat }
                            </li>
                        ) : (<Fragment></Fragment>) }
                    </ul>
                    <div class="card__main">
                        <h3 class="card__headline" dangerouslySetInnerHTML={ { __html: post.title.rendered } }></h3>
                        <div class="rte-content v-margin-collapse">
                            <p dangerouslySetInnerHTML={ { __html: post.content.rendered.replace( /[\r\n]+/gm, "" ).replace(/(<div class="card__image-credit"([^<]+)<\/div>)/ig, "").replace(/(<([^>]+)>)/ig, '').slice(0,460)+'...' } }></p>
                        </div>
                    </div>
                    <div class="card__footer">
                        <a href="#default" ref="cta" class="card__cta is-link" target="_self">
                            Mehr lesen
                            <i class="card__icon  fas fa-chevron-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}
function renderSinglePostAsListCard(post, props) {
    let image = '';
    if (post._embedded['wp:featuredmedia']) {
        image = post._embedded['wp:featuredmedia'][0].source_url;
    }
    let cat = '';
    if (post._embedded['wp:term']) {
        cat = post._embedded['wp:term'][0][0].name;
    }
    return (
        <div className="views-row">
            <div className="v-card">
                { image && image.length > 0 ? (
                    <div class="card__image-wrapper">
                        <figure class="figure">
                            <div class="figure__image-wrapper is-highlighted">
                                <div class="figure__image">
                                    <img src={ image } />
                                </div>
                            </div>
                        </figure>
                    </div>
                ) : (<Fragment></Fragment>) }                                            
                <div class="card__content">
                    <ul class="card__meta-data no-list-style">
                        <li class="card__meta-data-item is-date">
                            { new Date(post.date).toLocaleDateString('de-de', { year:"numeric", month:"numeric", day:"numeric"}) }
                        </li>
                        { cat && cat.length > 0 ? (
                            <li class="card__meta-data-item is-text">
                                { cat }
                            </li>
                        ) : (<Fragment></Fragment>) }
                    </ul>
                    <div class="card__main">
                        <h3 class="card__headline" dangerouslySetInnerHTML={ { __html: post.title.rendered } }></h3>
                        <div class="rte-content v-margin-collapse">
                            <p dangerouslySetInnerHTML={ { __html: post.content.rendered.replace( /[\r\n]+/gm, "" ).replace(/(<div class="card__image-credit"([^<]+)<\/div>)/ig, "").replace(/(<([^>]+)>)/ig, '').slice(0,460)+'...' } }></p>
                        </div>
                    </div>
                    <div class="card__footer">
                        <a href="#default" ref="cta" class="card__cta is-link" target="_self">
                            Mehr lesen
                            <i class="card__icon  fas fa-chevron-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}