/**
 * Block Blacklist
 */
wp.domReady(function() {
    let registeredBlocks = wp.blocks.getBlockTypes();

    wp.blocks.unregisterBlockType('core/verse');
    wp.blocks.unregisterBlockType('core/audio');
    wp.blocks.unregisterBlockType('core/group');
    // wp.blocks.unregisterBlockType('core/button');
    wp.blocks.unregisterBlockType('core/quote');
    wp.blocks.unregisterBlockType('core/file');
    wp.blocks.unregisterBlockType('core/cover');
    // wp.blocks.unregisterBlockType('core/freeform');
    wp.blocks.unregisterBlockType('core/html');
    wp.blocks.unregisterBlockType('core/code');
    wp.blocks.unregisterBlockType('core/preformatted');
    wp.blocks.unregisterBlockType('core/pullquote');
    wp.blocks.unregisterBlockType('core/table');
    wp.blocks.unregisterBlockType('core/media-text');
    wp.blocks.unregisterBlockType('core/more');
    wp.blocks.unregisterBlockType('core/nextpage');
    wp.blocks.unregisterBlockType('core/separator');
    wp.blocks.unregisterBlockType('core/calendar');
    wp.blocks.unregisterBlockType('core/archives');
    wp.blocks.unregisterBlockType('core/categories');
    wp.blocks.unregisterBlockType('core/latest-comments');
    wp.blocks.unregisterBlockType('core/latest-posts');
    wp.blocks.unregisterBlockType('core/rss');
    wp.blocks.unregisterBlockType('core/search');
    wp.blocks.unregisterBlockType('core/tag-cloud');
    wp.blocks.unregisterBlockType('core/query');

    const allowedEmbedVariants = ['youtube', 'vimeo'];
    wp.blocks.getBlockVariations('core/embed').forEach(variant => {
        if(!allowedEmbedVariants.includes(variant.name)) {
            wp.blocks.unregisterBlockVariation('core/embed', variant.name);
        }
    });
});
function isBlockRegistered(blockList, blockName) {
    if (blockList.filter(function(e) { return e.name === blockName; }).length > 0) {
        return true;
    }
    return false;
}