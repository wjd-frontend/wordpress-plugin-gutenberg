import './editor.scss';
import './style.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { 
    RichText,
    InnerBlocks,
    BlockControls,
    AlignmentToolbar,
    InspectorControls,
    MediaUpload
} = wp.blockEditor;
const {
    Fragment,
} = wp.element;
const { 
    Icon,    
    SelectControl,
    PanelBody,
    PanelRow,
	TextControl,
	Button,
    RangeControl
} = wp.components;

const iconEl = () => (
	<Icon
		icon={
			<svg width="24" height="24" role="img" aria-hidden="true" focusable="false">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M9,8a1,1,0,0,1,1-1h6a1,1,0,0,1,1,1v4a1,1,0,0,1-1,1h-1v3a1,1,0,0,1-1,1H8a1,1,0,0,1-1-1v-4a1,1,0,0,1,1-1h1V8zm2,3h4V9h-4v2zm2,2H9v2h4v-2z"></path>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M2,4.732A2,2,0,1,1,4.732,2h14.536A2,2,0,1,1,22,4.732v14.536A2,2,0,1,1,19.268,22H4.732A2,2,0,1,1,2,19.268V4.732zM4.732,4h14.536c.175.304.428.557.732.732v14.536a2.01,2.01,0,0,0-.732.732H4.732A2.01,2.01,0,0,0,4,19.268V4.732A2.01,2.01,0,0,0,4.732,4z"></path>
            </svg>
		}
	/>
);


registerBlockType( 'wjd/group', {
    title: __( 'Sektion' ),
    icon: iconEl,
    category: 'common',
    keywords: [
        'sektion',
        'gruppe',
        'abschnitt',
        'container'
    ],
    attributes: {
        mediaID: {
			type: 'number',
			src: 'attribute',
			selector: 'img',
			attribute: 'data-media-id',
		},
		mediaURL: {
			type: 'string',
			src: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
        title: {
            type: 'string',
        },        
        alignment: {
            type: 'string',
            default: 'center',
        },
        sectionType: {
            type: 'string',
            default: 'default',
        },
        cardHeight: {
            type: 'int',
            default: '',
        },
        parallaxSpeed: {
            type: 'int',
            default: 0.2,
        },
        sectionImageOrientation: {
            type: 'string',
            default: 'image-middle-center',
        },
    },
    edit: function( props ) {
        const {
            className,
            attributes: {
                mediaID,
				mediaURL,
                title,
                alignment,
                sectionType,
                cardHeight,
                parallaxSpeed,
                sectionImageOrientation
            },
            setAttributes,
        } = props;
        const onChangeAlignment = ( newAlignment ) => {
            props.setAttributes( { alignment: newAlignment === undefined ? 'none' : newAlignment } );
        };
        return (
            <div className={ className }>     
                <InspectorControls>
                    <PanelBody title="Style">
                        <PanelRow>
                            <SelectControl
                                label="Darstellung Sektion"
                                options={ [
                                    { label: 'Standard', value: 'default' },
                                    { label: 'Funky', value: 'funky' },
                                ] }
                                onChange={ ( value ) => { setAttributes( { sectionType: value } ) } }
                                value={ sectionType }
                            />
                        </PanelRow>
                        <PanelRow>
                            <TextControl
                                label="Karten-Bilder Höhen (px)"
                                value={ cardHeight }
                                type="number"
                                onChange={ (value) => {
                                    setAttributes( { cardHeight: value } )
                                } }
                                help="Setzt alle Bilder in vertikalen Karten dieser Sektion auf die gleiche Höhe. Leer lassen, damit Bilder die Höhe selbst bestimmen."
                            />
                        </PanelRow>                        
                        <PanelRow>
                            <SelectControl
                                label="Karten-Bilder Ausrichtung"
                                help="Setzt die Ausrichtung der Bilder in Karten, tritt nur in Effekt bei gesetzter Karten-Bilder Höhe"
                                options={ [
                                    { label: 'Oben Links', value: 'image-top-left' },
                                    { label: 'Oben Mitte', value: 'image-top-center' },
                                    { label: 'Oben Rechts', value: 'image-top-right' },
                                    { label: 'Mitte Links', value: 'image-middle-left' },
                                    { label: 'Mitte Mitte', value: 'image-middle-center' },
                                    { label: 'Mitte Rechts', value: 'image-middle-right' },
                                    { label: 'Unten Links', value: 'image-bottom-left' },
                                    { label: 'Unten Mitte', value: 'image-bottom-center' },
                                    { label: 'Unten Rechts', value: 'image-bottom-right' }
                                ] }
                                onChange={ ( value ) => { setAttributes( { sectionImageOrientation: value } ) } }
                                value={ sectionImageOrientation }
                            />
                        </PanelRow>
                    </PanelBody>
                    <PanelBody title="Hintergrund">
                        <PanelRow>
                            <MediaUpload
                                onSelect={ ( image ) => {
                                        setAttributes( {
                                            mediaID: image.id,
                                            mediaURL: image.url,
                                        } )
                                    }
                                }
                                type="image"
                                value={ mediaID }
                                render={ ( { open } ) => (
                                    <Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
                                        { ! mediaID ? __( 'Hintergrund wählen' ) : <img src={ mediaURL } alt={ __( 'Hintergrund wählen' ) } data-media-id={ mediaID } /> }
                                    </Button>
                                ) }
                            />
                        </PanelRow>
                        <PanelRow>                            
                            <RangeControl
                                label="Parallax Geschwindigkeit"
                                value={ parallaxSpeed }
                                onChange={ (value) => {
                                    setAttributes( { parallaxSpeed: value } )
                                } }
                                min={ 0 }
                                max={ 1 }
                                help="Setzt die Geschwidigkeit des Parallax Effekts - 0 = Bild ist statisch; 1 = Bild scrollt wie normal mit Seitengeschwindigkeit"
                                step={.1}
                            />
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>  
                <BlockControls>
                    <AlignmentToolbar
                        value={ alignment }
                        onChange={ onChangeAlignment }
                    />
                </BlockControls>
                {
                    sectionType == 'funky' ? (
                        <div style={{ backgroundImage: `url(${mediaURL})`}}>
                            <RichText
                                tagName="h3"
                                style={ { textAlign: alignment } }
                                placeholder={ __( 'Sektionsüberschrift' ) }
                                value={ title }
                                onChange={ ( value ) => setAttributes( { title: value } ) }
                                className="section-headline"
                            />
                            <InnerBlocks />
                        </div>
                    ) : (
                        <Fragment>
                            <RichText
                                tagName="h3"
                                style={ { textAlign: alignment } }
                                placeholder={ __( 'Sektionsüberschrift' ) }
                                value={ title }
                                onChange={ ( value ) => setAttributes( { title: value } ) }
                                className="section-headline"
                            />
                            <div style={{ backgroundImage: `url(${mediaURL})`}}>
                                <InnerBlocks />
                            </div>
                        </Fragment>                        
                    )
                }
            </div>
        );
    },
    save: function( props ) {
        return (
            <div>
                <InnerBlocks.Content />
            </div>
        );
    },
} );