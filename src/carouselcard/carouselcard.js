import './editor.scss';
import {useSelect} from '@wordpress/data';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { 
    BlockControls,
    MediaUpload,
    RichText,
    InspectorControls,
} = wp.blockEditor;
const {
    Fragment,
} = wp.element;
const {
    SelectControl,
    ColorPalette,
    ToolbarGroup,
    ToolbarButton,
    Button,
    PanelBody,
    PanelRow,
} = wp.components;

registerBlockType( 'wjd/carouselcard', {
    title: __( 'Karusell-Karte' ),
    icon: 'editor-table',
    category: 'common',
    keywords: [
        'carousel-card',
        'karussellkarte',
        'slide'
    ],
    attributes: {
        mediaID: {
			type: 'number',
			src: 'attribute',
			selector: 'img',
			attribute: 'data-media-id',
		},
		mediaURL: {
			type: 'string',
			src: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
        cardHeadline: {
            type: 'string'
        },
        cardContent: {
            type: 'string'
        },
        cardColor: {
            type: 'string',
            default: '#e5eaf4'
        },
        cardHighlightColor: {
            type: 'string',
            default: '#f9423a'
        },
        overrideHighlightColor: {
            type: 'boolean',
            default: false
        },
        cardImageOrientation: {
            type: 'string',
            default: 'image-center-center'            
        },
        slideIndex: {
            type: 'int',
            default: -1
        }
    },
    parent: ['wjd/carousel'],
    edit: function( props ) {
        const {
            className,
            attributes: {
                mediaID,
				mediaURL,
                cardHeadline,
                cardContent,
                cardColor,
                cardHighlightColor,
                overrideHighlightColor,
                cardImageOrientation,
                slideIndex
            },
            setAttributes,
            isSelected
        } = props;
        const colors = [
            { name: 'lightblue', color: '#e5eaf4' },
            { name: 'blue', color: '#013493' },
            { name: 'transparent', color: '#00000000' },
        ];
        const highlghtColors = [
            { name: 'red', color: '#f9423a' },
            { name: 'orange', color: '#f8a102' },
            { name: 'green', color: '#41d4ae' },
        ];
        const {index} = useSelect( select => {
            const {getBlockIndex} = select( 'core/block-editor' );
            return {
                index: getBlockIndex( props.clientId )
            }
        } );
        setAttributes( { slideIndex: index } )
        if (!overrideHighlightColor) {
            switch (index % 3) {
                case 0:
                    setAttributes( { cardHighlightColor: '#f9423a' } )
                    break;        
                case 1:
                    setAttributes( { cardHighlightColor: '#f8a102' } )
                    break;        
                case 2:
                    setAttributes( { cardHighlightColor: '#41d4ae' } )
                    break;        
                default:
                    break;
            }
        }
        return (
            <div className={ className }>
                <div class={`card-wrap color-palette-${findWithAttr(colors, 'color', cardColor)}`} style={{borderTop: `5px solid ${cardHighlightColor}`}}>
                    <Fragment>
                        <InspectorControls key='inspector'>
                            <PanelBody title="Karteneigenschaften">
                                <PanelRow>
                                    <ColorPalette 
                                        colors={ colors }
                                        value={ cardColor }
                                        onChange={ (value) => { setAttributes( { cardColor: value } ) } }
                                        disableCustomColors={true}
                                        clearable={false}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <ColorPalette 
                                        colors={ highlghtColors }
                                        value={ cardHighlightColor }
                                        onChange={ (value) => { setAttributes( { overrideHighlightColor: true, cardHighlightColor: value } ) } }
                                        disableCustomColors={true}
                                        clearable={false}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label="Bildausrichtung wählen"
                                        value={ cardImageOrientation }
                                        options={ [
                                            { label: 'Oben Links', value: 'image-top-left' },
                                            { label: 'Oben Mitte', value: 'image-top-center' },
                                            { label: 'Oben Rechts', value: 'image-top-right' },
                                            { label: 'Mitte Links', value: 'image-middle-left' },
                                            { label: 'Mitte Mitte', value: 'image-middle-center' },
                                            { label: 'Mitte Rechts', value: 'image-middle-right' },
                                            { label: 'Unten Links', value: 'image-bottom-left' },
                                            { label: 'Unten Mitte', value: 'image-bottom-center' },
                                            { label: 'Unten Rechts', value: 'image-bottom-right' },
                                        ] }
                                        onChange={ (value) => { setAttributes( { cardImageOrientation: value } ) } }
                                    />
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                        <BlockControls>
                            <ToolbarGroup>
                                <MediaUpload
                                    allowedTypes={ [ 'image' ] }
                                    value={ mediaID }
                                    onSelect={ ( image ) => setAttributes( { mediaID: image.id, mediaURL: image.url } ) }
                                    render={ ( { open } ) => (
                                        <ToolbarButton
                                            className="components-toolbar__control"
                                            label={ __( 'Bild ändern' ) }
                                            icon={ 'edit' }
                                            onClick={ open }
                                        />
                                    ) }
                                />
                                <ToolbarButton
                                    className="components-toolbar__control"
                                    label={ __( 'Bild entfernen' ) }
                                    icon={ 'no' }
                                    onClick={ () => setAttributes( { mediaID: undefined } ) }
                                />
                            </ToolbarGroup>
                        </BlockControls>
                        <div class={`card-media`}>
                            <MediaUpload
                                onSelect={ ( image ) =>
                                    setAttributes( {
                                        mediaID: image.id,
                                        mediaURL: image.url,
                                    } ) }
                                type="image"
                                value={ mediaID }
                                render={ ( { open } ) => (
                                    <Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
                                        { ! mediaID ? __( 'Kartenbild wählen' ) : <img src={ mediaURL } alt={ __( 'Kartenbild wählen' ) } data-media-id={ mediaID } /> }
                                    </Button>
                                ) }
                            />
                        </div>
                        <div class="card-content">
                            <Fragment>
                                <div class="card-headline">
                                    <span className='card-no' style={{color: cardHighlightColor}}>{ slideIndex + 1 }</span>
                                    <RichText
                                        tagName="h3"
                                        className= "card__headline"
                                        placeholder={ 'Kartenüberschrift' }
                                        value={cardHeadline}
                                        onChange={ ( value ) => setAttributes( { cardHeadline: value } ) }
                                    />
                                    <RichText
                                        tagName="p"
                                        className= "card__content"
                                        placeholder={ 'Karteninhalt' }
                                        value={cardContent}
                                        onChange={ ( value ) => setAttributes( { cardContent: value } ) }
                                    />
                                </div>
                            </Fragment>
                        </div>
                    </Fragment>
                </div>
            </div>
        );
    },
    save: function( props ) {
        return ( props );
    },
} );

function findWithAttr(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}
