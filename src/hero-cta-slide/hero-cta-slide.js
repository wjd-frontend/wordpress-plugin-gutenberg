import './editor.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { 
    BlockControls,
    MediaUpload,
	URLInput,
    InspectorControls,
} = wp.blockEditor;
const {
    Fragment,
} = wp.element;
const {
    Button,
    CheckboxControl,
    ToolbarGroup,
    ToolbarButton,
    PanelBody,
    PanelRow,
} = wp.components;

registerBlockType( 'wjd/hero-cta-slide', {
    title: __( 'Hero-CTA-Slide' ),
    icon: 'editor-table',
    category: 'common',
    keywords: [
        'cta-slide',
        'hero',
        'call to action'
    ],
    attributes: {
        mediaID: {
			type: 'number',
			src: 'attribute',
			selector: 'img',
			attribute: 'data-media-id',
		},
		mediaURL: {
			type: 'string',
			src: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
        slideUrl: {
            type: 'string'
        },
        newTab: {
            type: 'boolean',
            default: false
        }
    },
    parent: ['wjd/hero-cta'],
    edit: function( props ) {
        const {
            className,
            attributes: {
                mediaID,
				mediaURL,
                slideUrl,
                newTab
            },
            setAttributes
        } = props;
        return (
            <div className={ className }>
                <div class={`slide-wrap`}>
                    <Fragment>
                        <InspectorControls key='inspector'>
                            <PanelBody title="Linkeigenschaften">
                                <PanelRow>
                                    <CheckboxControl
                                        label="Link in neuem Tab öffnen?"
                                        checked={ newTab }
                                        onChange={ (value) => { setAttributes( { newTab: value } )} }
                                    />
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                        <BlockControls>
                            <ToolbarGroup>
                                <MediaUpload
                                    allowedTypes={ [ 'image' ] }
                                    value={ mediaID }
                                    onSelect={ ( image ) => setAttributes( { mediaID: image.id, mediaURL: image.url } ) }
                                    render={ ( { open } ) => (
                                        <ToolbarButton
                                            className="components-toolbar__control"
                                            label={ __( 'Bild ändern' ) }
                                            icon={ 'edit' }
                                            onClick={ open }
                                        />
                                    ) }
                                />
                                <ToolbarButton
                                    className="components-toolbar__control"
                                    label={ __( 'Bild entfernen' ) }
                                    icon={ 'no' }
                                    onClick={ () => setAttributes( { mediaID: undefined } ) }
                                />
                            </ToolbarGroup>
                        </BlockControls>
                        <div class={`card-media`}>
                            <MediaUpload
                                onSelect={ ( image ) =>
                                    setAttributes( {
                                        mediaID: image.id,
                                        mediaURL: image.url,
                                    } ) }
                                type="image"
                                value={ mediaID }
                                render={ ( { open } ) => (
                                    <Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
                                        { ! mediaID ? __( 'Slidebild wählen' ) : <img src={ mediaURL } alt={ __( 'Slidebild wählen' ) } data-media-id={ mediaID } /> }
                                    </Button>
                                ) }
                            />
                        </div>
                        <div class="slide-content">
                            <URLInput
                                value={ slideUrl }
                                onChange={ ( value ) => setAttributes( { slideUrl: value } ) }
                            />
                        </div>
                    </Fragment>
                </div>
            </div>
        );
    },
    save: function( props ) {
        return ( props );
    },
} );